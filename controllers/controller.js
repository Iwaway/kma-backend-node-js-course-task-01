const сontroller = {
    square(req, res) {
        const number = parseFloat(req.body);
    
        if (isNaN(number)) {
          return res.status(400).send('Invalid number!');
        }

        const square = number * number;
    
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json({ number, square });
    },

    reverse(req, res) {
        if (!req.body) {
          return res.status(400).send('Text is required!');
        }
    
        const reversedText = req.body.split('').reverse().join('');
    
        res.setHeader('Content-Type', 'text/plain');
        res.status(200).send(reversedText);
    },

    date(req, res) {
        const { year, month, day } = req.params;
    
        const date = new Date(`${year}-${month}-${day}`);
    
        if (isNaN(date.getTime())) {
          return res.status(400).json({ error: 'Invalid date!' });
        }
    
        const weekDay = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][date.getDay()];
    
        const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    
        const currentDate = new Date(new Date().setHours(0, 0, 0, 0));

        const difference = Math.floor((Math.abs(currentDate.getTime() - date.getTime())) / (1000 * 60 * 60 * 24));
    
        res.setHeader('Content-Type', 'application/json');
        res.json({ weekDay, isLeapYear, difference });
      }
  };

module.exports = сontroller;