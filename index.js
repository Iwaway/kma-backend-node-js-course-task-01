require('dotenv').config();

const express = require('express');
const routes = require("./routes/routes");

const app = express();
const PORT = process.env.PORT || 56201;

app.use(routes);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
