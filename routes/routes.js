const express = require('express');
const router = express.Router();
const controller = require('../controllers/controller');

router.use(express.json());

router.post('/square', express.text(), controller.square);
router.post('/reverse', express.text(), controller.reverse);
router.get('/date/:year/:month/:day', controller.date);

module.exports = router;